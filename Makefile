SHELL = /bin/bash

CODE ?= src tests
VENV_BIN ?= .venv/bin/
PYTHON ?= $(VENV_BIN)python
PIP_COMPILE ?= $(VENV_BIN)pip-compile
PIP_SYNC ?= $(VENV_BIN)pip-sync
BLACK ?= $(VENV_BIN)black
MYPY ?= $(VENV_BIN)mypy
PYTEST ?= $(VENV_BIN)pytest

.PHONY: default clean venv lint reformat tests

default:
	echo 'default: not implemented!'

clean:
	find $(CODE) -iname '*.pyc' -exec rm {} \;
	find $(CODE) -iname '__pycache__' -exec rm -r {} \;
	rm -rf .mypy_cache .pytest_cache

venv:
	[ -d .venv ] || python3 -m venv .venv
	$(PYTHON) -m pip install -U pip setuptools wheel pip-tools

requirements.txt: venv
	$(PIP_COMPILE) requirements.in


requirements-dev.txt: venv
	$(PIP_COMPILE) --generate-hashes requirements-dev.in


pip-sync: requirements.txt
	$(PIP_SYNC) --generate-hashes requirements.txt


pip-sync-dev: requirements-dev.txt requirements.txt
	$(PIP_SYNC) requirements-dev.txt requirements.txt

lint:
	$(BLACK) --check -- $(CODE)
	$(MYPY) -- $(CODE)

reformat:
	$(BLACK) -- $(CODE)

tests:
	$(PYTEST)
