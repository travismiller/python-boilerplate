# Python Boilerplate

https://gitlab.com/travismiller/python-boilerplate

## Dependencies

- Make
- Python

## Development

Add requirements to `requirements.in` and `requirements-dev.in`.

See https://github.com/jazzband/pip-tools

```console
$ make pip-sync-dev
```

See the [Makefile](Makefile) for additional useful make targets.

## License

This software is licensed under the permissive [MIT license](LICENSE).
